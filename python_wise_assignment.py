# -*- coding: utf-8 -*-
"""python_wise_assignment

Automatically generated by Colaboratory.

Original file is located at
    https://colab.research.google.com/drive/1vjqxnfovwixWokG8AzUvPJ8qBEBsx5kc
"""

#1
str="My name is rabiya warsi"
s=str.split()
max=0
for i in s:
  if len(i)>max:
    max=len(i)
    word=i
print("the longest string lenght {} is {}".format(word,len(i)))

#2
str=input("enyer a string:")
size=len(str)
if size%2==0:
    half=size//2
    if str[0:half]==str[half:] and str==str[::-1]:
      print("it is symmetrical and palindrome")
    else:
      print("it is not symmetrical and alindrome")

#3
str=input("enter a string")
alphabet="abcdefghijklmnopqrstuvwxyz"
flag=True
for char in alphabet:
  if char not in str.lower():
      flag=False
if(flag==True):
    print("it is a program")
else:
    print("it is not a program")

#4
print("enter a hyphen separated sequence of words:")
list=[n for n in input().split('-')]
list.sort()
print("the sorted list is:")
print('-'.join(list))

#5
def is_prime(n):
  for i in range(2,n):
    if n%i==0:
      return False
  return True
def generate_twins(start,end):
  for i in range(start,end):
    j=i+2
    if is_prime(i) and is_prime(j):
      print("{:d} and {:d}".format(i,j))
generate_twins(2,20)

#6
def double_characters(input_string): 
    doubled_string = ""
    for char in input_string:
        doubled_string += char * 2
    return doubled_string
input_string1 = "now"
input_string2 = "123a!"
print(double_characters(input_string1))
print(double_characters(input_string2))

#7
def fibonacci_numbers(num):
  if num == 0:
    return 0
  elif num == 1:
    return 1
  else:
    return fibonacci_numbers(num-2)+fibonacci_numbers(num-1)
n=int(input("enter the range:"))
for i in range(0, n):
  print(fibonacci_numbers(i), end=" ")

#8
n=int(input("enter a number"))
even_count = 0
odd_count = 0
while (n > 0):
        rem = n % 10
        # if condition is true then increment even_count
        if (rem % 2 == 0):
            even_count +=1
        # increment odd count
        else:
          odd_count +=1

        n = int(n / 10)
print( "Even count : " , even_count)
print("\nOdd count : " , odd_count)

#9
num=int(input("enter the number:"))
sum=0
for i in range (1,num):
  if num%i==0:
    sum+=i
if num==sum:
  print("it is a perfect number")
else:
  print("it is not a perfect number")

#10
str=input("enter a string:")
s=str.split()
for i in s:
    if len(i)%2==0:
        word=i
        print("{}".format(word))

#11
n=int(input("enter number of rows"))
for i in range(1, n+1):
    for j in range(0, n-i+1):
        print(' ',end='')
    C = 1
    for j in range(1, i+1):
      print(' ',C, sep='',end='')
      C =C * (i - j)  //j
    print()

#12
user_input = input("enter up to 72 characters containing integers and floats separated by commas")
user_input = user_input.replace(" ","")
numbers = user_input.split(",")
total = 0
for num in numbers:
    total += float(num)
print("User input: ", user_input)
print("sum: ", total)

#13
num=int(input("enter a number"))
temp=num
c=0
while 1:
  p=1
  c=c+1
  while temp!=0:
    rem=temp%10
    p=p*rem
    temp=int(temp/10)
  if p<10:
    print(p)
    print(c)
    break
  temp=p

#14
def multiply_digits(n):
    product = 1
    for digit in str(n):
        product *= int(digit)
    return product

def multiplicative_digital_root(n):
    while n > 9:
        n = multiply_digits(n)
    return n

def multiplicative_persistence(n):
    count = 0
    while n > 9:
        n = multiply_digits(n)
        count += 1
    return count
n=int(input("enter a number:"))
mdr = multiplicative_digital_root(n)
mp = multiplicative_persistence(n)
print("Multiplicative digital root:", mdr)
print("Multiplicative persistence:",mp)

#15
import math
def calculate_area(shape, length, radius):
    if shape == "SQR":
        diagonal = length * math.sqrt(2)
        side = diagonal / math.sqrt(2)
        area = side ** 2
        return area 
    elif shape == "CI":
        circle_area = math.pi * radius ** 2
        square_area = length ** 2
        intersection_area = (circle_area * 2) - (math.pi * radius ** 2)
        area = square_area - intersection_area
        return area
    else:
        return -1
print("Area of square=",calculate_area("SQR",10,0))
print("Area of circle=",calculate_area("CI",10,1))

#16
O=list(map(int,input().split()))
i=1
l=len(O)
while(i<=1):
  if i==O[i-1]:
    print("HIT "+str(i))
    O=O[i:]+O[:i-1]
    l-=1
    i=1
  else:
    i+=1

#17
def coordinates(x1,y1,x2,y2,x3,y3,x,y):
  delta=(y2-y3)*(x1-x3)+(x3-x2)*(y1-y3)
  u=((y2-y3)*(x-x3)+(x3-x2)*(y-y3))/delta
  v=((y3-y1)*(x-x3)+(x1-x3)*(y-y3))/delta
  w=1-u-v
  return u,v,w
def point_inside_triangle(x1,y1,x2,y2,x3,y3,x,y):
  u,v,w=coordinates(x1,y1,x2,y2,x3,y3,x,y)
  return u>=0 and v>=0 and w>=0
n=int(input("enter the n value:"))
x1,y1=map(float,input().split())
x2,y2=map(float,input().split())
x3,y3=map(float,input().split())
for i in range(n):
  x,y=map(float,input().split())
  if point_inside_triangle(x1,y1,x2,y2,x3,y3,x,y):
    print("INSIDE")
  else:
    print("OUTSIDE")

#18
s=input("enter text:")
words=s.split()
sen=""
l=80
for i in words:
  sen=sen+i+" "
  if len(sen)>1:
    sen=sen.replace(i,"")
    print(sen)
    l=len(sen)
    sen=i+" "
  elif len(sen)<1 and i==words[-1]:
    sen=sen.replace(i,"")
    print(sen+"\n"+i)
  else:
    pass

#19
num_subjects = int(input("Enter the number of subjects: "))
subjects = []
students = []
marks = []
for i in range(num_subjects):
    subject_name = input(f" enter the name of subject {i+1}: ")
    subjects.append(subject_name)
    student_name = input(f"Enter the name of student who scored highest in {subject_name}: ")
    students.append(student_name)
    mark = int(input(f"Enter the marks scored by {student_name} in {student_name}:"))
    marks.append(mark)
final_list = []
for i in range(num_subjects):
    subject_dict = {}
    subject_dict[subjects[i]] = {students[i]: marks[i]}
    final_list.append(subject_dict)
print(f"Number of subjects = {num_subjects}")
print(f"subjects = {subjects}")
print(f"students = {students}")
print(f"Final List = {final_list}")

#20
r=str.upper(input("enter a roman number"))
d={'I':1,'V':5,'X':10,'L':50,'C':100,'D':500,'M':1000}
sum=0
for i in range (0,len(r)):
  if i+1!=len(r) and d[r[i]]<d[r[i+1]]:
    sum=sum-d[r[i]]
  else:
    sum=sum+d[r[i]]
print(sum)

#21
list1=[2,6,8,9,8,11,256,8]
list2=[]
for i in range (0,len(list1)):
  for j in range(i+1,len(list1)):
    if list1[i]==list1[j] and list1[i] not in list2:
      list2.append(list1[i])
list3=list(set(list1)-set(list2))
list3.sort()
print("new nums= ",list3)
print("len {}".format(len(list3)))
list4=[]
for i in range(0,len(list1)):
  for j in range(len(list2)):
    if list1[i]==list2[j]:
      list1[i]="+"
print("newest nums:",'[%s}'%','.join(map(str,list1)))